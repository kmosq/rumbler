CC=gcc

PREFIX=/usr/local

rumbler: src/main.c
	$(CC) -I src $(shell sdl2-config --cflags) src/main.c $(shell sdl2-config --libs) -o rumbler

tester: src/tester.c
	$(CC) src/tester.c -o tester

install: rumbler
	install -D rumbler $(PREFIX)/bin/rumbler

