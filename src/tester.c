#include <stdio.h>

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>

int main() {
	char *fifo_path = "/tmp/rumbler-fifo";
	char *fifo_path_env = getenv("RUMBLER_FIFO_PATH");
	if ( fifo_path_env ) {
		fifo_path = fifo_path_env;
	}
	int fifo = open("/tmp/rumbler-fifo", O_WRONLY);
	struct {
		float l;
		float r;
	} ins = { 0.f, 1.f };
	for (int i = 0; i <= 10; i++) {
		printf("l %f r %f\n", ins.l, ins.r);
		write(fifo, &ins, sizeof(ins));
		ins.l += 0.1;
		ins.r -= 0.1;
		usleep(0.5 * 1000000);
	}
	return 0;
}

