#include <stdio.h>
#include <string.h>

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <malloc.h>
#include <stdint.h>

#include <linux/limits.h>

#include "SDL2/SDL.h"

struct {
	SDL_GameController *gaco;
	int fifo_fd;
	char *fifo_path;
} state;

void cleanup() {
	if ( state.gaco ) {
		SDL_GameControllerClose(state.gaco);
	}
	if ( state.fifo_fd != -1 ) {
		close(state.fifo_fd);
	}
	if ( state.fifo_path ) {
		remove(state.fifo_path);
	}
	SDL_Quit();
}

void sig_handle(int signo) {
	fprintf(stderr, "rumbler: received signal %i\nrumbler: shutting down\n", signo);
	cleanup();
	exit(1);
}

int main(int argc, char **argv) {
	int rv = 0;

	state.gaco = NULL;
	state.fifo_fd = -1;
	state.fifo_path = NULL;
	
	signal(SIGINT, &sig_handle);
	signal(SIGTERM, &sig_handle);

	if ( argc < 2 ) {
		fprintf(stderr, "No event path given\n");
		return 1;
	}

	char *path = argv[1];
	
	float rumble_strength_multiplier = 1.f;

	if ( argc > 2 ) {
		sscanf(argv[2], "%f", &rumble_strength_multiplier);
	}

	if ( SDL_Init( SDL_INIT_GAMECONTROLLER ) < 0 ) {
		fprintf(stderr, "Couldn't initialize SDL\n");
		return 1;
	}

	printf("Searching for Game Controller %s\n", path);
	SDL_GameController *gaco = NULL;
	for ( int i = 0; i < SDL_NumJoysticks(); i++ ) {
		if ( ! SDL_IsGameController(i) ) {
			continue;
		}
		gaco = SDL_GameControllerOpen(i);
		const char *ipath = SDL_GameControllerPath(gaco);
		fprintf(stderr, "Game Controller %i: \"%s\"\n", i, ipath);
		if ( strcmp(path, ipath) == 0 ) {
			break;
		}
		SDL_GameControllerClose(gaco);
		gaco = NULL;
	}
	
	if ( gaco == NULL ) {
		fprintf(stderr, "No matching game controller found\n");
		return 1;
	}

	printf("Found gamepad %s\n", SDL_GameControllerName(gaco));
	state.gaco = gaco;

	if ( ! SDL_GameControllerHasRumble(gaco) ) {
		fprintf(stderr, "Game controller doesn't have rumble support\n");
		goto err_close_and_exit;
	}

	
	char *fifo_path = "/tmp/rumbler-fifo";
	char *fifo_path_env = getenv("RUMBLER_FIFO_PATH");

	if ( fifo_path_env ) {
		fifo_path = fifo_path_env;
	}

	state.fifo_path = fifo_path;

	int fifo_err = mkfifo(fifo_path, 0666);
	if ( fifo_err != 0 ) {
		fprintf(stderr, "Failed to init fifo at %s (retval %i)\n", fifo_path, fifo_err);
		goto err_close_and_exit;
	}

	int fifo_fd;

	fifo_fd = open(fifo_path, O_RDWR);
	state.fifo_fd = fifo_fd;
	if ( fifo_fd == -1 ) {
		fprintf(stderr, "Failed to open fifo %s for reading\n", fifo_path);
		goto err_close_and_exit;
	}

	struct {
		float low_freq;
		float high_freq;
	} instruction;

	while ( 1 ) {
		read(fifo_fd, &instruction, sizeof(instruction));
		uint16_t low = (uint16_t)(instruction.low_freq * rumble_strength_multiplier * (float)UINT16_MAX);
		uint16_t high = (uint16_t)(instruction.high_freq * rumble_strength_multiplier * (float)UINT16_MAX);
		SDL_GameControllerRumble(gaco, low, high, 10000);
	}

	goto close_and_exit;
err_close_and_exit:;
	fprintf(stderr, "SDL_GetError(): \"%s\"\n", SDL_GetError());
	rv = 1;	
close_and_exit:;
	cleanup();
	return rv;
}
